import processing.serial.*;

String[] portNames = Serial.list();
for (int i = 0; i < portNames.length; i++) {
  println("[" + i + "] " + portNames[i]);
}
