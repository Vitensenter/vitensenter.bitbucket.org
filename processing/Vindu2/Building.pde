public class Building extends SceneObject {
  private PImage[] sprites;
  private int state;
  private float xpos, ypos;

  public Building(PImage[] sprites, float xpos, float ypos) {
    this.sprites = sprites;
    this.xpos = xpos;
    this.ypos = ypos;
    state = 0;
  }

  public void update() {
    // Change state if environment has changed here
  }

  public void draw() {
    image(sprites[state], xpos, ypos);
  }
}
