public class SceneManager {
  // Environment
  private int environmentState;

  private PImage sky, darkSky, mountain, grass;
  private PImage[] tree;
  private PImage[] house;
  private PImage[] water;
  private PImage[] snow;

  // Scene stuff
  ArrayList<SceneObject> sceneObjects;

  public SceneManager() {
    sceneObjects = new ArrayList<SceneObject>();
    environmentState = 0;

    sky = loadImage("sky1.png");
    darkSky = loadImage("sky2.png");
    mountain = loadImage("mountains.png");
    grass = loadImage("grass.png");

    tree = new PImage[4];
    tree[0] = loadImage("tree1.png");
    tree[1] = loadImage("tree2.png");
    tree[2] = loadImage("tree3.png");
    tree[3] = loadImage("tree4.png");

    house = new PImage[4];
    house[0] = loadImage("house1.png");
    house[1] = loadImage("house2.png");
    house[2] = loadImage("house3.png");
    house[3] = loadImage("house4.png");

    water = new PImage[4];
    water[0] = loadImage("water1.png");
    water[1] = loadImage("water2.png");
    water[2] = loadImage("water3.png");
    water[3] = loadImage("water4.png");

    snow = new PImage[4];
    snow[0] = loadImage("snow1.png");
    snow[1] = loadImage("snow2.png");
    snow[2] = loadImage("snow3.png");

    sceneObjects.add(new Tree(820, 570, tree, this));
    sceneObjects.add(new Tree(1020, 620, tree, this));
    sceneObjects.add(new Tree(920, 600, tree, this));
    sceneObjects.add(new Tree(1100, 590, tree, this));
    sceneObjects.add(new Tree(1050, 610, tree, this));
    sceneObjects.add(new Tree(1020, 550, tree, this));
    sceneObjects.add(new Tree(1220, 630, tree, this));
    sceneObjects.add(new Tree(800, 590, tree, this));
    sceneObjects.add(new Tree(1150, 540, tree, this));
    sceneObjects.add(new Tree(870, 600, tree, this));

    sceneObjects.add(new House(500, 600, house, this));
    sceneObjects.add(new House(590, 570, house, this));
    sceneObjects.add(new House(600, 640, house, this));
    sceneObjects.add(new House(690, 590, house, this));
  }

  public void incrementState() {
    environmentState++;
    if (environmentState > 3) environmentState = 3;
  }

  public void decrementState() {
    environmentState--;
    if (environmentState < 0) environmentState = 0;
  }

  public int getEnvironmentState() {
    return environmentState;
  }

  public void update() {
    for (SceneObject s : sceneObjects) {
      s.update();
    }
  }

  public void draw() {
    // Draw background elements
    image(sky, 0, 0);
    image(mountain, 0, 0);
    image(grass, 0, 0);
    image(water[0], 0, 0);
    image(snow[0], 0, 0);

    switch (environmentState) {
      case 0:
      image(darkSky, 0, 0);
      image(mountain, 0, 0);
      break;
      case 1:
      image(sky, 0, 0);
      image(mountain, 0, 0);
      image(grass, 0, 0);
      image(water[0], 0, 0);
      image(snow[2], 0, 0);
      break;
      case 2:
      image(sky, 0, 0);
      image(mountain, 0, 0);
      image(grass, 0, 0);
      image(water[0], 0, 0);
      image(snow[0], 0, 0);
      break;
      case 3:
      image(sky, 0 ,0);
      image(mountain, 0, 0);
      image(grass, 0, 0);
      image(water[0], 0, 0);
      break;
    }

    text("State: " + environmentState, 20, 20);

    // Draw scene objects
    for (SceneObject s : sceneObjects) {
      s.draw();
    }
  }
}
