class Tree extends SceneObject {
  float xpos, ypos;
  PImage[] sprites;
  int state;
  int targetState;
  SceneManager sceneManager;
  Integrator integrator;

  public Tree(int xpos, int ypos, PImage[] sprites, SceneManager sceneManager) {
    super();
    this.xpos = xpos;
    this.ypos = ypos;
    this.sprites = sprites;
    this.sceneManager = sceneManager;
    integrator = new Integrator(0);

    state = sprites.length - 1;

    // Debug print
    // println("New SceneObject: " + this.toString());
  }

  public void update() {
    state = sceneManager.getEnvironmentState();

    if (state < 0) {
      state = 0;
    } else if (state > sprites.length -1) {
      state = sprites.length - 1;
    }
  }

  public void draw() {
    switch (state) {
      case 0:
      break;
      case 1:
      image(sprites[state-1], xpos, ypos);
      break;
      case 2:
      image(sprites[state-1], xpos, ypos);
      break;
      case 3:
      image(sprites[state], xpos, ypos);
      break;
      default:
      image(sprites[3], xpos, ypos);
      break;
    }


    // image(sprites[state], xpos, ypos);
  }

  public String toString() {
    return "Tree @ [" + xpos + "," + ypos + "]";
  }
}
