public class Cloud {
  PImage sprite;
  float xpos, ypos;
  float speed;

  public Cloud(PImage sprite, float ypos, float speed) {
    this.sprite = sprite;
    this.xpos = -sprite.width;
    this.ypos = ypos;
    this.speed = speed;
  }

  public void update() {
    xpos += speed;

    // Hardcoded loop to 720p res
    // To be changed
    if (xpos > 1280) {
      xpos = -sprite.width;
    }
  }

  public void draw() {
    image(sprite, xpos, ypos);
  }
}
