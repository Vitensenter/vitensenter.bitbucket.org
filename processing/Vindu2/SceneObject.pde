public abstract class SceneObject {
  /**
  * Updates the object state.
  */
  public void update() {};

  /**
  * Draws the object to the scene.
  */
  public void draw() {};
}
