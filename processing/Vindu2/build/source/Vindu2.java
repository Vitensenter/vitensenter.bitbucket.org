import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Vindu2 extends PApplet {

SceneManager sm;

public void setup() {
  
  sm = new SceneManager();

}

public void draw() {
  sm.update();
  sm.draw();
}

public void keyPressed() {
  if (key == 'a') {
    sm.incrementState();
  }
  if (key == 's') {
    sm.decrementState();
  }
}
public class Building extends SceneObject {
  private PImage[] sprites;
  private int state;
  private float xpos, ypos;

  public Building(PImage[] sprites, float xpos, float ypos) {
    this.sprites = sprites;
    this.xpos = xpos;
    this.ypos = ypos;
    state = 0;
  }

  public void update() {
    // Change state if environment has changed here
  }

  public void draw() {
    image(sprites[state], xpos, ypos);
  }
}
public class Cloud {
  PImage sprite;
  float xpos, ypos;
  float speed;

  public Cloud(PImage sprite, float ypos, float speed) {
    this.sprite = sprite;
    this.xpos = -sprite.width;
    this.ypos = ypos;
    this.speed = speed;
  }

  public void update() {
    xpos += speed;

    // Hardcoded loop to 720p res
    // To be changed
    if (xpos > 1280) {
      xpos = -sprite.width;
    }
  }

  public void draw() {
    image(sprite, xpos, ypos);
  }
}
class House extends SceneObject {
  float xpos, ypos;
  PImage[] sprites;
  int state;
  int targetState;
  SceneManager sceneManager;
  Integrator integrator;

  public House(int xpos, int ypos, PImage[] sprites, SceneManager sceneManager) {
    super();
    this.xpos = xpos;
    this.ypos = ypos;
    this.sprites = sprites;
    this.sceneManager = sceneManager;
    integrator = new Integrator(0);

    state = sprites.length - 1;

    // Debug print
    // println("New SceneObject: " + this.toString());
  }

  public void update() {
    state = sceneManager.getEnvironmentState();

    if (state < 0) {
      state = 0;
    } else if (state > sprites.length -1) {
      state = sprites.length - 1;
    }
  }

  public void draw() {
    switch (state) {
      case 0:
      break;
      case 1:
      image(sprites[state-1], xpos, ypos);
      break;
      case 2:
      image(sprites[state-1], xpos, ypos);
      break;
      case 3:
      image(sprites[state-1], xpos, ypos);
      break;
      default:
      image(sprites[3], xpos, ypos);
      break;
    }

    // image(sprites[state], xpos, ypos);
  }

  public String toString() {
    return "House @ [" + xpos + "," + ypos + "]";
  }
}
class Integrator {

  final float DAMPING = 0.5f;
  final float ATTRACTION = 0.2f;

  float value;
  float vel;
  float accel;
  float force;
  float mass = 1;

  float damping = DAMPING;
  float attraction = ATTRACTION;
  boolean targeting;
  float target;


  Integrator() { }


  Integrator(float value) {
    this.value = value;
  }


  Integrator(float value, float damping, float attraction) {
    this.value = value;
    this.damping = damping;
    this.attraction = attraction;
  }


  public void set(float v) {
    value = v;
  }


  public void update() {
    if (targeting) {
      force += attraction * (target - value);      
    }

    accel = force / mass;
    vel = (vel + accel) * damping;
    value += vel;

    force = 0;
  }


  public void target(float t) {
    targeting = true;
    target = t;
  }


  public void noTarget() {
    targeting = false;
  }
}
public class SceneManager {
  // Environment
  private int environmentState;

  private PImage sky, darkSky, mountain, grass;
  private PImage[] tree;
  private PImage[] house;
  private PImage[] water;
  private PImage[] snow;

  // Scene stuff
  ArrayList<SceneObject> sceneObjects;

  public SceneManager() {
    sceneObjects = new ArrayList<SceneObject>();
    environmentState = 0;

    sky = loadImage("sky1.png");
    darkSky = loadImage("sky2.png");
    mountain = loadImage("mountains.png");
    grass = loadImage("grass.png");

    tree = new PImage[4];
    tree[0] = loadImage("tree1.png");
    tree[1] = loadImage("tree2.png");
    tree[2] = loadImage("tree3.png");
    tree[3] = loadImage("tree4.png");

    house = new PImage[4];
    house[0] = loadImage("house1.png");
    house[1] = loadImage("house2.png");
    house[2] = loadImage("house3.png");
    house[3] = loadImage("house4.png");

    water = new PImage[4];
    water[0] = loadImage("water1.png");
    water[1] = loadImage("water2.png");
    water[2] = loadImage("water3.png");
    water[3] = loadImage("water4.png");

    snow = new PImage[4];
    snow[0] = loadImage("snow1.png");
    snow[1] = loadImage("snow2.png");
    snow[2] = loadImage("snow3.png");

    sceneObjects.add(new Tree(820, 570, tree, this));
    sceneObjects.add(new Tree(1020, 620, tree, this));
    sceneObjects.add(new Tree(920, 600, tree, this));
    sceneObjects.add(new Tree(1100, 590, tree, this));
    sceneObjects.add(new Tree(1050, 610, tree, this));
    sceneObjects.add(new Tree(1020, 550, tree, this));
    sceneObjects.add(new Tree(1220, 630, tree, this));
    sceneObjects.add(new Tree(800, 590, tree, this));
    sceneObjects.add(new Tree(1150, 540, tree, this));
    sceneObjects.add(new Tree(870, 600, tree, this));

    sceneObjects.add(new House(500, 600, house, this));
    sceneObjects.add(new House(590, 570, house, this));
    sceneObjects.add(new House(600, 640, house, this));
    sceneObjects.add(new House(690, 590, house, this));
  }

  public void incrementState() {
    environmentState++;
    if (environmentState > 3) environmentState = 3;
  }

  public void decrementState() {
    environmentState--;
    if (environmentState < 0) environmentState = 0;
  }

  public int getEnvironmentState() {
    return environmentState;
  }

  public void update() {
    for (SceneObject s : sceneObjects) {
      s.update();
    }
  }

  public void draw() {
    // Draw background elements
    image(sky, 0, 0);
    image(mountain, 0, 0);
    image(grass, 0, 0);
    image(water[0], 0, 0);
    image(snow[0], 0, 0);

    switch (environmentState) {
      case 0:
      image(darkSky, 0, 0);
      image(mountain, 0, 0);
      break;
      case 1:
      image(sky, 0, 0);
      image(mountain, 0, 0);
      image(grass, 0, 0);
      image(water[0], 0, 0);
      image(snow[2], 0, 0);
      break;
      case 2:
      image(sky, 0, 0);
      image(mountain, 0, 0);
      image(grass, 0, 0);
      image(water[0], 0, 0);
      image(snow[0], 0, 0);
      break;
      case 3:
      image(sky, 0 ,0);
      image(mountain, 0, 0);
      image(grass, 0, 0);
      image(water[0], 0, 0);
      break;
    }

    text("State: " + environmentState, 20, 20);

    // Draw scene objects
    for (SceneObject s : sceneObjects) {
      s.draw();
    }
  }
}
public abstract class SceneObject {
  /**
  * Updates the object state.
  */
  public void update() {};

  /**
  * Draws the object to the scene.
  */
  public void draw() {};
}
class Tree extends SceneObject {
  float xpos, ypos;
  PImage[] sprites;
  int state;
  int targetState;
  SceneManager sceneManager;
  Integrator integrator;

  public Tree(int xpos, int ypos, PImage[] sprites, SceneManager sceneManager) {
    super();
    this.xpos = xpos;
    this.ypos = ypos;
    this.sprites = sprites;
    this.sceneManager = sceneManager;
    integrator = new Integrator(0);

    state = sprites.length - 1;

    // Debug print
    // println("New SceneObject: " + this.toString());
  }

  public void update() {
    state = sceneManager.getEnvironmentState();

    if (state < 0) {
      state = 0;
    } else if (state > sprites.length -1) {
      state = sprites.length - 1;
    }
  }

  public void draw() {
    switch (state) {
      case 0:
      break;
      case 1:
      image(sprites[state-1], xpos, ypos);
      break;
      case 2:
      image(sprites[state-1], xpos, ypos);
      break;
      case 3:
      image(sprites[state], xpos, ypos);
      break;
      default:
      image(sprites[3], xpos, ypos);
      break;
    }


    // image(sprites[state], xpos, ypos);
  }

  public String toString() {
    return "Tree @ [" + xpos + "," + ypos + "]";
  }
}
  public void settings() {  size(1280, 720); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Vindu2" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
