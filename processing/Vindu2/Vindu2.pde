SceneManager sm;

public void setup() {
  size(1280, 720);
  sm = new SceneManager();

}

public void draw() {
  sm.update();
  sm.draw();
}

public void keyPressed() {
  if (key == 'a') {
    sm.incrementState();
  }
  if (key == 's') {
    sm.decrementState();
  }
}
