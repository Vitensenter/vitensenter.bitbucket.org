import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.serial.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Vindu5 extends PApplet {



PImage[] states; // Graphics for the various states
Integrator[] interpolators; // Interpolators for fading between states
int state; // Current state
int previousState;

// https://learn.sparkfun.com/tutorials/connecting-arduino-to-processing
Serial myPort; // Serial handler
String serialVal; // Value read from serial
Serial mp3slave;

public void setup() {
   // Set fullscreen
  // size(1280, 720); // Set size
  state = 0; // Set initial state to 0
  states = new PImage[4]; // Initialize graphics array
  interpolators = new Integrator[4]; // Initialize interpolator array

  // Set up serial communication
  // setupSerial();

  // Initialize interpolators
  for (int i = 0; i < states.length; i++) {
    states[i] = loadImage("state" + i + ".png");
    interpolators[i] = new Integrator(0, 0.5f, 0.1f);
  }

  if (width != 1280 || height != 720) {
    for (PImage img : states) {
      img.resize(width, height);
    }
  }

  background(0);
}

public void draw() {
  // Get state from serial
  // getStateFromSerial();

  // Update interpolators
  for (int i = 0; i < interpolators.length; i++) {
    interpolators[i].update();
  }

  // Set interpolator targets
  if (state == 0) {
    interpolators[1].target(0);
    interpolators[2].target(0);
    interpolators[3].target(0);
  } else if (state == 1) {
    interpolators[1].target(255);
    interpolators[2].target(0);
    interpolators[3].target(0);
  } else if (state == 2) {
    interpolators[1].target(255);
    interpolators[2].target(255);
    interpolators[3].target(0);
  } else if (state == 3) {
    interpolators[1].target(255);
    interpolators[2].target(255);
    interpolators[3].target(255);
  }

  // Draw first state
  noTint();
  image(states[0], 0, 0);

  // Draw second state
  tint(255, interpolators[1].value);
  image(states[1], 0, 0);

  // Draw third state
  tint(255, interpolators[2].value);
  image(states[2], 0, 0);

  // Draw fourth state
  tint(255, interpolators[3].value);
  image(states[3], 0, 0);

  // Debug text
  noTint();
  stroke(255);
  text("State: " + state + "\nFPS: " + frameRate, 5, 13);
}

public void setupSerial() {
  String portName = Serial.list()[0]; // Change to match port, this may change
  myPort = new Serial(this, portName, 9600);
  serialVal = "";
  String mp3portName = Serial.list()[1];//Dette maa sjekkes/tilpasses
  mp3slave = new Serial(this, mp3portName, 9600);
}

public void getStateFromSerial() {
  // If data is available
  if (myPort.available() > 0) {
    serialVal = myPort.readStringUntil('\n'); // Read and store
  }

  if (serialVal != null) {
    if (serialVal.contains("s0")) {
      state = 0;
      if (state != previousState) mp3slave.write("s0\n");
    } else if (serialVal.contains("s1")) {
      state = 1;
      if (state != previousState) mp3slave.write("s1\n");
    } else if (serialVal.contains("s2")) {
      state = 2;
      if (state != previousState) mp3slave.write("s2\n");
    } else if (serialVal.contains("s3")) {
      state = 3;
      if (state != previousState) mp3slave.write("s3\n");
    }
  }
  previousState = state;
}

// Debug keyboard commands for changing state
public void keyPressed() {
  if (key == 'a') {
    decrementState();
  }
  if (key == 's') {
    incrementState();
  }
}

public void incrementState() {
  state++;
  if (state > states.length - 1) state = states.length -1;
}

public void decrementState() {
  state--;
  if (state < 0) state = 0;
}
class Integrator {

  final float DAMPING = 0.5f;
  final float ATTRACTION = 0.2f;

  float value;
  float vel;
  float accel;
  float force;
  float mass = 1;

  float damping = DAMPING;
  float attraction = ATTRACTION;
  boolean targeting;
  float target;


  Integrator() { }


  Integrator(float value) {
    this.value = value;
  }


  Integrator(float value, float damping, float attraction) {
    this.value = value;
    this.damping = damping;
    this.attraction = attraction;
  }


  public void set(float v) {
    value = v;
  }


  public void update() {
    if (targeting) {
      force += attraction * (target - value);      
    }

    accel = force / mass;
    vel = (vel + accel) * damping;
    value += vel;

    force = 0;
  }


  public void target(float t) {
    targeting = true;
    target = t;
  }


  public void noTarget() {
    targeting = false;
  }
}
  public void settings() {  fullScreen(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Vindu5" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
