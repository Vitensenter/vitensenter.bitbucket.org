/*
Denne filen kjører på et slikt oppsett:  http://www.geeetech.com/wiki/index.php/File:VS1053-sch.jpg

Gjennom seriell kommunikasjon lytter MP3Slave etter "SX\n" og endrer tilstand til X (X kan bare være 0-9 for nå).

SD-Kort oppsett  for å forbinde globus til 
"track001.mp3"  inneholder    Lyden av å vri på en kran/ventil.
"track002.mp3"  inneholder    Lav av susing av gass gjennom rør.   
"track003.mp3"  inneholder    Lyd av snøstorm (https://www.freesound.org/people/cobratronik/sounds/117136/).        
"track004.mp3"  inneholder    Lyd av fuglekvitter (https://www.freesound.org/people/Dynamicell/sounds/17548/).    
"track003.mp3"  inneholder    Lyd av skogbrann (https://www.freesound.org/people/urupin/sounds/122616/).        

SD-Kort oppsett 
"track001.mp3" spilles av en gang når en kommer inn i state 0. (wilhelm scream)
"track002.mp3" spilles av en gang når en kommer inn i state 1.
"track003.mp3" spilles av en gang når en kommer inn i state 2.
"track004.mp3" spilles av en gang når en kommer inn i state 3. (wilhelm scream)

"track011.mp3" loopes i state 0.
"track012.mp3" loopes i state 1.
"track013.mp3" loopes i state 2.
"track014.mp3" loopes i state 3. 

*/

#include <SPI.h>
#include <SdFat.h>
#include <SdFatUtil.h> 
#include <SFEMP3Shield.h>

SdFat sd;
SFEMP3Shield MP3player;

byte state;
byte prevState;
byte loopTrack;
String inData;
String temp;

void setup() {
   Serial.begin(9600);  
   setupMp3();
   Serial.println("MP3Slave klar.");
}

void loop() {
  
  while (Serial.available() > 0)
    {
        char recieved = Serial.read();
        
        
        if (recieved == '\n') {
          if(inData && inData[0] == 's' && inData.length()==2 && isDigit(inData[1])){ //NB endre length krav om vi får mer enn 9 states
            Serial.print("\tMP3Slave Received: ");
            Serial.println(inData);
      
            prevState=state;
            state=inData[1]-48; //fordi char 48 er lik string 0
            if(prevState!=state){
             /* MP3player.stopTrack();  
              playTrack(state+1);
              loopTrack=state+11;*/
            
               switch(state){
                  case 0:
                  Serial.println("\tMP3Slave er i state 0");
                  MP3player.stopTrack();                  
                  playTrack(1);
                  loopTrack=11;
                  break;
                  
                  case 1:
                  Serial.println("\tMP3Slave er i state 1");
                  MP3player.stopTrack();
                  loopTrack=14;
                  break;
                  
                  case 2:
                  Serial.println("\tMP3Slave er i state 2");
                  MP3player.stopTrack();
                  loopTrack=14;
                  break;
                  
                  case 3:
                  Serial.println("\tMP3Slave er i state 3");
                  MP3player.stopTrack();
                  playTrack(4);
                  loopTrack=11;
                  break;
                }
                }
          }
                inData = ""; // Clear recieved buffer
          
        } else {
          inData += recieved; 
        }
    } 
    
      if(loopTrack && !MP3player.isPlaying()){ //spill av kontinuerlig
        playTrack(loopTrack);
      }
    
    
    
    
    

}

void playTrack(int n){
  int mp3ErrorCode= MP3player.playTrack(n);
    switch (mp3ErrorCode){   //Håndter feil. Vi kjenner bare til feilkode 0,1,2,6
      case 0:
      //Alt gikk fint
      return;
      break;
      
      case 1:
      Serial.println("\tFor .begin: '*Failure of SdFat to initialize physical contact with the SdCard'");
      Serial.println("\tForrige fil spilles fortsatt av og skapte muligvis en feil.");
      break;
      
      case 2:
      Serial.println("\tFor .begin: '*Failure of SdFat to start the SdCard's volume'");
      temp = "";
      if(n<10){
        temp += "00";
      } else if(n<100){
         temp += "0";
      }
      Serial.println("\tFant ikke filen 'track"+temp+String(n)+".mp3'");
      break;
      
      case 3:
      Serial.println("\tFor .begin: '*Failure of SdFat to mount the root directory on the volume of the SdCard'");
      break;
      
      case 4:
      Serial.println("\tFor .begin: 'Other than default values were found in the SCI_MODE register.'");
      break;
      
      case 5:
      Serial.println("\tFor .begin: 'SCI_CLOCKF did not read back and verify the configured value.'");
      break;
      
      case 6:
      Serial.println("\tFor .begin: 'Patch was not loaded successfully. This may result in playTrack errors'");
      Serial.println("\tMp3Kortet ble ikke satt opp riktig. Vi prover igjen.");
      setupMp3();
      playTrack(n);  //NB kan bli evig loop om det ikke løser problemet og samme feilkode gjentas. Men i det tilfellet kjenner vi ingen annen mulig løsning.
      break;
    }
}

void setupMp3(){
  //start the shield
  if(!sd.begin(SD_SEL, SPI_HALF_SPEED)) sd.initErrorHalt();
  MP3player.begin(); 
}

void queueTrack(int n){//TODO denne er ikke testet.
//Denne while-loopen gjentas mens en lydfil spilles av
  while(MP3player.isPlaying()){
    delay(300);  //Hvor lenge skal vi vente mens forrige lydfil spiller?  NB Her er delay en grei løsning, men det kan være nødvendig å endre senere.
  }
  
  //Når ingen lydfiler spilles av kan vi spille den neste.
  playTrack(n);
}