/**
  INF2260 H2016 - Vitensenteret
*/

#include <Servo.h>

// Relékontroll-pins
#define HOTAIR_PIN 2 // varmluft
#define ATOMIZER_PIN 3 // atomizer
#define FAN_IN_PIN 4 // vifte røyk inn
#define FAN_OUT_PIN 5 // vifte røyk ut
#define LED1_NORMAL_PIN 6 // normal lys solsiden
#define LED1_HOT_PIN 7 // varmt lys solsiden
#define LED2_NORMAL_PIN 8 // normal lys mørksiden
#define LED2_COLD_PIN 9 // kaldt lys mørksiden

// Pins til andre ting
#define VALVE_PIN A0 // kran til styring av drivhusgasser / røykmaskin
#define HOTAIR_SERVO_PIN 10 // servoen som styrer retninga på varmlufta

Servo hotairServo;

// Timing til varmluft og røykmaskin
// TODO: Finn ut fornuftige verdier for disse

unsigned long hotAirOnTime;     // Tiden vifteovnen er på (ms)
unsigned long hotAirOffTime;    // Tiden vifteovnen er av (ms)
unsigned long atomizerOnTime;   // Tiden atomizeren er på (ms)
unsigned long atomizerOffTime;  // Tiden atomizeren er av (ms)
unsigned long inFanOnTime;      // Tiden vifta som blåser røyk inn er på (ms)
unsigned long inFanOffTime;     // Tiden vifta som blåser røyk inn er av (ms)
unsigned long outFanOnTime;     // Tiden vifta som blåser røyk ut er på (ms)
unsigned long outFanOffTime;    // Tiden vifta som blåser røyk ut er av (ms)

unsigned long hotAirTime;       // Tidspunktet varmlufta sist endret status
unsigned long atomizerTime;     // Tidspunktet atomizeren sist endret status
unsigned long inFanTime;        // Tidspunktet inn-vifta sist endret status
unsigned long outFanTime;       // Tidspunktet ut-vifta sist endret status

boolean hotAirOn;               // True hvis varmlufta er på
boolean atomizerOn;             // True hvis atomizeren er på
boolean inFanOn;                // True hvis inn-vifta er på
boolean outFanOn;               // True hvis ut-vifta er på

int systemState;                // Status på systemet 0-3
int prevSystemState;            // Forrige status på systemet

int valveReading;               // Verdien lest fra potentiometeret

unsigned long currentTime;      // Tiden når loop() starter

// Debugting
unsigned long lastDebugPrintTime;
unsigned long debugPrintTiming;

void setup() {
  Serial.begin(9600);
  Serial.println("Starting...");
  setupPins();
  setupServo();

  inFanOnTime = 500; // 0.5 s
  inFanOffTime = 2500;  // 2.5 s

  atomizerOnTime = 2000; // 2 s
  atomizerOffTime = 5000; // 5 s

  // Debugting
  lastDebugPrintTime = 0;
  debugPrintTiming = 1000; // Print debugting en gang i sekundet
}

void loop() {
  // Sjekk tiden
  currentTime = millis();

  // Sjekk kranposisjon
  valveReading = map(analogRead(VALVE_PIN), 70, 777, 1023, 0);
  //debugPrintln("valveReading" + (String) valveReading);

  // Sjekk hvilken status systemet skal settes i
  if (valveReading < 256) {
    // Systemet er av
    systemState = 0;
  } else if (valveReading >= 256 && valveReading < 512) {
    // Systemet er 1/3 på
    systemState = 1;
  } else if (valveReading >= 512 && valveReading < 768) {
    // Systemet er 2/3 på
    systemState = 2;
  } else if (valveReading >= 768) {
    // Systemet er 3/3 på
    systemState = 3;
  }

  //Om noe har endret seg
  if(prevSystemState != systemState) {

    //Spill av lydeffekt for feedback
    playSound(systemState);

    //Endre lys
    setLightMode(systemState);

    // Sett timing på tinga som trenger det
    setTimings(systemState);

    // Send status på systemet over serial til Pi
    Serial.println("s" + (String)systemState);

    //Forslag: Skal vi tømme litt røyk om vi går fra en modus med mye røyk,
    //til en med lite røyk?
    if(prevSystemState>systemState){
      //TODO kjør outFan litt
    }


  }


  setSmokeMode(systemState);


  // TODO:Sjekk om ting skal skrus av/på iht. timing

  // debugPrintln("systemState" + (String) systemState);

  // Lagre forrige systemState
  prevSystemState = systemState;

  delay(100); // For stabilitetens skyld
}

/**
  Skrur av og på ting iht. til timing
*/
void setSmokeMode (int state) {
  if (state == 0) {
    // Alt skal av untatt ut-vifta
    digitalWrite(ATOMIZER_PIN, LOW);
    digitalWrite(FAN_IN_PIN, LOW);
    digitalWrite(FAN_OUT_PIN, LOW);
    // Bør ut-vifta skrus på med passelig intervaller her?
  } else {

    // Varmluft
    if (hotAirOn) {
      // Varmluft er på
      if (hotAirTime + hotAirOnTime < currentTime) {
        digitalWrite(HOTAIR_PIN, LOW);
        hotAirOn = false;
        hotAirTime = currentTime;
      }
    } else if (!hotAirOn) {
      // Varmluft er av
      if (hotAirTime + hotAirOffTime < currentTime) {
        digitalWrite(HOTAIR_PIN, HIGH);
        hotAirOn = true;
        hotAirTime = currentTime;
      }
    }

    // Atomizer
    if (atomizerOn) {
      // Atomizeren er på
      if (atomizerTime + atomizerOnTime < currentTime) {
        digitalWrite(ATOMIZER_PIN, LOW);
        atomizerOn = false;
        atomizerTime = currentTime;
      }
    } else if (!atomizerOn) {
      // Atomizeren er av
      if (atomizerTime + atomizerOffTime < currentTime) {
        digitalWrite(ATOMIZER_PIN, HIGH);
        atomizerOn = true;
        atomizerTime = currentTime;
      }
    }

    // Inn-vifte
    if (inFanOn) {
      // Inn-vifte er på
      if (inFanTime + inFanOnTime < currentTime) {
        digitalWrite(FAN_IN_PIN, LOW);
        inFanOn = false;
        inFanTime = currentTime;
      }
    } else if (!inFanOn) {
      // Inn-vifte er av
      if (inFanTime + inFanOffTime < currentTime) {
        digitalWrite(FAN_IN_PIN, HIGH);
        inFanOn = true;
        inFanTime = currentTime;
      }
    }

    // Ut-vifte
    if (outFanOn) {
      // Ut-vifte er på
      if (outFanTime + outFanOnTime < currentTime) {
        digitalWrite(FAN_OUT_PIN, LOW);
        outFanOn = false;
        outFanTime = currentTime;
      }
    } else if (!outFanOn) {
      // Ut-vifte er av
      if (outFanTime + outFanOffTime < currentTime) {
        digitalWrite(FAN_OUT_PIN, HIGH);
        outFanOn = true;
        outFanTime = currentTime;
      }
    }

  }
}

/**
  Setter timingen for når delene styrt gjennom reléet slås av/på
*/
void setTimings (int state) {
  switch (state) {
    case 0:
      hotAirOnTime = 0;
      hotAirOffTime = 5000;
      atomizerOnTime = 0;
      atomizerOffTime = 2500;
      inFanOnTime = 0;
      inFanOffTime = 2500;
      outFanOnTime = 3000;
      outFanOffTime = 3000;
    break;

    case 1:
      hotAirOnTime = 0;
      hotAirOffTime = 7000;
      atomizerOnTime = 1000;
      atomizerOffTime = 5000;
      inFanOnTime = 500;
      inFanOffTime = 10000;
      outFanOnTime = 2000;
      outFanOffTime = 3000;
    break;

    case 2:
      hotAirOnTime = 0;
      hotAirOffTime = 9000;
      atomizerOnTime = 3000;
      atomizerOffTime = 1000;
      inFanOnTime = 500;
      inFanOffTime = 5000;
      outFanOnTime = 1000;
      outFanOffTime = 3000;
    break;

    case 3:
      hotAirOnTime = 0;
      hotAirOffTime = 12000;
      atomizerOnTime = 5000;
      atomizerOffTime = 0;
      inFanOnTime = 500;
      inFanOffTime = 2500;
      outFanOnTime = 0;
      outFanOffTime = 10000;
    break;
  }
}

/**
  Varmluft til solsiden
*/
void openA() {
  hotairServo.write(90);
}

/**
  Varmluft til mørkesiden
*/
void openB() {
  hotairServo.write(140);
}

/**
  Varmluft til begge sidene
*/
void openBoth() {
  hotairServo.write(115);
}

/**
  Spiller av lyd som feedback ved endring av modus
*/
void playSound(int state){
  //TODO lag metoden
}

/**
  Endrer lys til aa stemme med modus
*/
void setLightMode (int state) {
    //TODO pass på at lysene er riktig
    switch (state) {
     case 0:
      digitalWrite(LED1_NORMAL_PIN, LOW);
      digitalWrite(LED1_HOT_PIN, HIGH);
      digitalWrite(LED2_NORMAL_PIN, LOW);
      digitalWrite(LED2_COLD_PIN, HIGH);
     break;

     case 1:
     digitalWrite(LED1_NORMAL_PIN, HIGH);
     digitalWrite(LED1_HOT_PIN, LOW);
     digitalWrite(LED2_NORMAL_PIN, HIGH);
     digitalWrite(LED2_COLD_PIN, LOW);
      break;

     case 2:
     digitalWrite(LED1_NORMAL_PIN, HIGH);
     digitalWrite(LED1_HOT_PIN, LOW);
     digitalWrite(LED2_NORMAL_PIN, HIGH);
     digitalWrite(LED2_COLD_PIN, LOW);
     break;

     case 3:
      digitalWrite(LED1_NORMAL_PIN, HIGH);
      digitalWrite(LED1_HOT_PIN, LOW);
      digitalWrite(LED2_NORMAL_PIN, HIGH);
      digitalWrite(LED2_COLD_PIN, LOW);
     break;
    }
}


void setupServo() {
  hotairServo.attach(HOTAIR_SERVO_PIN);
  openBoth();
}

void setupPins() {
  // Sett relékontroll-pins til output
  pinMode(HOTAIR_PIN, OUTPUT);
  pinMode(ATOMIZER_PIN, OUTPUT);
  pinMode(FAN_IN_PIN, OUTPUT);
  pinMode(FAN_OUT_PIN, OUTPUT);
  pinMode(LED1_NORMAL_PIN, OUTPUT);
  pinMode(LED1_HOT_PIN, OUTPUT);
  pinMode(LED2_NORMAL_PIN, OUTPUT);
  pinMode(LED2_COLD_PIN, OUTPUT);
}

void debugPrintln(String message) {
  if (lastDebugPrintTime + debugPrintTiming < currentTime) {
    Serial.println(message);
    lastDebugPrintTime = currentTime;
  }
}
