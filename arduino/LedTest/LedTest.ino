#define RED_PIN 3
#define GREEN_PIN 5
#define BLUE_PIN 6

int redValue;
int greenValue;
int blueValue;

void setup() {
  // Test ting her
    pinMode(RED_PIN, OUTPUT);
    pinMode(GREEN_PIN, OUTPUT);
    pinMode(BLUE_PIN, OUTPUT);

    redValue = 255 * 4;
    greenValue = 128 * 4;
    blueValue = 0 * 4;
}

void loop() {
  //analogWrite(RED_PIN, redValue);
  //analogWrite(GREEN_PIN, greenValue);
  //analogWrite(BLUE_PIN, blueValue);
  //digitalWrite(RED_PIN, HIGH);
  //digitalWrite(GREEN_PIN, HIGH);
  //digitalWrite(BLUE_PIN, HIGH);
  //delay(50);

  analogWrite(BLUE_PIN, 0);
  delay(1000);
  analogWrite(BLUE_PIN, 512);
  delay(1000);
  analogWrite(BLUE_PIN, 1023);
  delay(1000);

  //digitalWrite(BLUE_PIN, HIGH);
  //delay(1000);
  //digitalWrite(BLUE_PIN, LOW);
  //delay(1000);
}
