jQuery(document).ready(function($){
	var timelineBlocks = $('.cd-timeline-block'),
		offset = 0.8;

	//hide timeline blocks which are outside the viewport
	hideBlocks(timelineBlocks, offset);

	//on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function(){
		(!window.requestAnimationFrame) 
			? setTimeout(function(){ showBlocks(timelineBlocks, offset); }, 100)
			: window.requestAnimationFrame(function(){ showBlocks(timelineBlocks, offset); });
	});

	function hideBlocks(blocks, offset) {
		blocks.each(function(){
			( $(this).offset().top > $(window).scrollTop()+$(window).height()*offset ) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		});
	}

	function showBlocks(blocks, offset) {
		blocks.each(function(){
			( $(this).offset().top <= $(window).scrollTop()+$(window).height()*offset && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
		});
	}
$('.my-slider').unslider();

	$('#toProt').on('click', function(event) {
    var target = $('#prot');
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 400);
    }
});

setupFancybox();

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
var index;
var elms = document.getElementsByClassName("egenPara");
for (index = elms.length-1; index >= 0; index--) {
	 elms[index].parentNode.removeChild(elms[index])
}

} 



});
function setupFancybox(){
	$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			
		

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});
//egen for video
   jQuery(".fancy_video").fancybox({
        // set type of content (we are building the HTML5 <video> tag as content)
        type: "html",
        // other API options
        scrolling: "no",
        fitToView: false,
        autoSize: false,
        beforeLoad: function () {
            // build the HTML5 video structure for fancyBox content with specific parameters
            this.content = "<video id='video_player' src='" + this.href + "' poster='" + $(this.element).data("poster") + "' width='360' height='360' controls='controls' preload='none' ></video>";
            // set fancyBox dimensions
            this.width = 360; // same as video width attribute
            this.height = 360; // same as video height attribute
        },
        afterShow: function () {
            // initialize MEJS player
            var $video_player = new MediaElementPlayer('#video_player', {
                defaultVideoWidth: this.width,
                defaultVideoHeight: this.height,
                success: function (mediaElement, domObject) {
                    _player = mediaElement; // override the "mediaElement" instance to be used outside the success setting
                    _player.load(); // fixes webkit firing any method before player is ready
                    _player.play(); // autoplay video (optional)
                    _player.addEventListener('playing', function () {
                        _isPlaying = true;
                    }, false);
                } // success
            });
        },
        beforeClose: function () {
            // if video is playing and we close fancyBox
            // safely remove Flash objects in IE
            if (_isPlaying && navigator.userAgent.match(/msie [6-8]/i)) {
                // video is playing AND we are using IE
                _player.remove(); // remove player instance for IE
                _isPlaying = false; // reinitialize flag
            };
        }
    });
	
	function resizePath(){
  var reBy =document.getElementById("specpath").width/400;
  document.getElementById("mySVG").width=document.getElementById("specpath").width;
  var theD="M"+73 *reBy+" "+95 *reBy+
  " L"+212*reBy+" "+160*reBy+" L"+296*reBy+" "+76 *reBy+" L"+341*reBy+" "+164*reBy+
  " L"+80 *reBy+" "+220*reBy+" L"+296*reBy+" "+76 *reBy+" L"+361*reBy+" "+174*reBy+
  " L"+255*reBy+" "+162*reBy+" L"+166*reBy+" "+340*reBy+" L"+212*reBy+" "+310*reBy+
  " L"+166*reBy+" "+350*reBy+" L"+212*reBy+" "+325*reBy+" L"+252*reBy+" "+370*reBy+
  " L"+230*reBy+" "+320*reBy+" L"+260*reBy+" "+361*reBy+" L"+237*reBy+" "+318*reBy+
  " L"+269*reBy+" "+360*reBy+" L"+245*reBy+" "+318*reBy+" L"+282*reBy+" "+369*reBy+
  " L"+255*reBy+" "+311*reBy+" L"+315*reBy+" "+352*reBy+" L"+295*reBy+" "+85 *reBy+
  " L"+330*reBy+" "+160*reBy+" L"+243*reBy+" "+302*reBy+" L"+294*reBy+" "+353*reBy+
  " L"+341*reBy+" "+85 *reBy+" L"+368*reBy+" "+149*reBy+" L"+226*reBy+" "+300*reBy+
  " C"+124*reBy+" "+206*reBy+" "+212*reBy+" "+407*reBy+" "+259*reBy+" "+369*reBy+
  " C"+283*reBy+" "+400*reBy+" "+313*reBy+" "+400*reBy+" "+325*reBy+" "+370*reBy;
document.getElementById("triangle").setAttribute("d", theD );
setSkrL();
}
function showPath(){
	document.getElementById("pathdiv").style.visibility = 'visible';   
}
function hidePath(){
	document.getElementById("pathdiv").style.visibility = 'hidden';   
}

var scrollpercent;
var prevSP;
function setSkrL(){
var triangle = document.getElementById("triangle");
var length = triangle.getTotalLength();
triangle.style.strokeDasharray = length;
triangle.style.strokeDashoffset = length;
window.addEventListener("scroll", function(){
	var prevSP=scrollpercent;
	/**
	 0 på 0
	 liten bs 0.040P 111.1L
	 Møte 0.078P 192.67L
	 Dom 0.133P 450.365
	 obs Spø 0.182445 på 634.7
	(ana 0.2325P på 770 )
	tanke 0.359 926.23
	utvik 0.41444p på 108925
	eval 0.875p på 2006.75
	ev2 0.9726p på 2009.4
100 på 2521
function through (0,0), (0.04, 111.1), (0.078,192.67), (0.133,540.365), (0.18244,634.7), (0.2325,770), (0.359, 926.23), (0.41444, 1089.25), (0.875, 2006.75), (0.9726, 2009.4) and (1,2521)
	 
3853.43 x^3 - 5962.71 x^2 + 4500.91 x - 34.2305 (cubic)

	 */

	
 scrollpercent = (document.body.scrollTop - document.getElementById("cd-timeline").offsetTop  ) / (document.getElementById("toProt").offsetTop - document.getElementById("cd-timeline").offsetTop -document.getElementById("sist").scrollHeight );
 if(scrollpercent<=0){
	scrollpercent=0;
	if(prevSP!=scrollpercent){
		hidePath();
	}
 } else  if(scrollpercent>=1){
	scrollpercent=1;
	
 } else {
	if(prevSP==0&&prevSP!=scrollpercent){
		showPath();
	}
//---
var PL = [
  [0,        0],
  [0.040,    111.1],
  [0.078,    192.76],
  [0.133,    450.365],
  [0.182445, 634.7],
  [0.2325,   770],
  [0.359,    926.23],
  [0.4144,   1089.25],
  [0.875,    2033],//nest siste eval 2092.33
  [0.918,    2238],//analyse og arbed mellom evals
  [0.9726,   2330.07], //siste eval
  [1,        2393]
];


var draw;

var pCount =0;
while(pCount+1<PL.length){
	if(scrollpercent<PL[pCount+1][0]){
		var relP=(scrollpercent-PL[pCount][0])/(PL[pCount+1][0]-PL[pCount][0]);
		draw = PL[pCount][1]+((PL[pCount+1][1]-PL[pCount][1])*relP);
	break;
	}
	pCount+=1;
}




//---
 }
 // var draw = length * scrollpercent;
 //var draw = (3853.43*scrollpercent*scrollpercent*scrollpercent) - (5963.71*scrollpercent*scrollpercent)+(4500.91*scrollpercent)-32.2305;
  
  if(draw>2939 | !draw){
	draw=2393;
}
  triangle.style.strokeDashoffset = length - draw;

  /**
   Start: document.getElementById("cd-timeline").offsetTop
   nå: document.body.scrollTop 
   Slutt: document.getElementById("toProt").offsetTop
   % = nå - start / Slutt - start - siste
   */
});


}
resizePath();
	
}