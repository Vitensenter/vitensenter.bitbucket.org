//Denne filen inneholder fungerende kode for å styre ventilen. 
//Vi har ennå ikke bestemt oss for å bruke arduino som "hjerne" til prototypen, men mulige fremgangsmåter beskrives her.

//Bibliotek
#include <Servo.h>

//Globale
Servo ventilServo;

void setup() {
  setUpVentil();
}

void setUpVentil(){
  ventilServo.attach(9);  
  openBoth();
}

void openA(){
  ventilServo.write(90);
}

void openBoth(){
  ventilServo.write(115);
}

void openB(){
  ventilServo.write(140);
}

void loop() {
  /*
  
  Metode 1: (mer samsvar mellom vindu og globus, men den antar at mengde røyk er lett å endre og måle)
   1. Sjekk røykmengde
   2. Si fra til Pi om røykmengde
   3. Endre lys
   4. Spill av lydeffekt.
   5. Juster ventil
   
   a. Sjekk kranposisjon
   b. juster mengde røyk (hvor lenge atomizer skal kjøre || tøm luft)
   
  Metode 2: (krever ikke røyksensor, men kan føre til misforståelser om røykmengde ikke er lett å styre)
   a. Sjekk kranposisjon
     om den har endret seg nok:
       b. Juster mengde røyk (hvor lenge atomizer skal kjøre || tøm luft).
       c. Endre lys.
       d. Si fra til Pi om kranposisjon 
       e. spill av lydeffekt.
       f. juster ventil.
  */
}